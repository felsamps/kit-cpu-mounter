

import static org.junit.Assert.*;
import kitcpu.defs.CPUDefinitions;
import kitcpu.mounter.Instruction;

import org.junit.Test;

public class InstructionTest {

	@Test
	public void testLDR() {
		String testLine = "pula: LDR A valorB ;comentário";

		Instruction.initIsaSizes();
		Instruction.initIsaOpcodes();
		Instruction.initRegCodes();
				
		Instruction i = new Instruction(testLine);
		
		assertEquals("pula", i.getLabel());
		assertEquals("LDR", i.getInst());
		assertEquals("a", i.getReg());
		assertEquals("valorB", i.getMem());
	}

	@Test
	public void testHLT() {
		String testLine = "hlt";

		Instruction.initIsaSizes();
		Instruction.initIsaOpcodes();
		Instruction.initRegCodes();
		
		Instruction i = new Instruction(testLine);
				
		assertEquals(null, i.getLabel());
		assertEquals("hlt", i.getInst());
		assertEquals(null, i.getReg());
		assertEquals(null, i.getMem());
	}
	
	
}
