package kitcpu.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import kitcpu.mounter.Instruction;
import kitcpu.mounter.Mounter;
import kitcpu.exceptions.IllegalMounterMacroException;


public class Main {

	
	public static void main(String[] args) {
		
		try {	
			JFileChooser fc = new JFileChooser("res/");
			int returned = fc.showOpenDialog(null);
		    
			if (returned == JFileChooser.APPROVE_OPTION) {
		        File sourceFile;
		        sourceFile = new File(fc.getSelectedFile().getPath());
		        Mounter mnt = new Mounter(sourceFile);
		        mnt.firstPass();
                        mnt.secondPass();
                        
                        mnt.report();
		    }			
		}
		catch(FileNotFoundException fnfe) {
			JOptionPane.showMessageDialog(null, "Erro ao abrir o arquivo!");
		}
		catch(IllegalMounterMacroException imme) {
			JOptionPane.showMessageDialog(null, imme.getErrorMessage());
		}
	    
	    
	}

}
