package kitcpu.defs;

import kitcpu.mounter.Instruction;

public class CommonRegex {
	public static final String COMMENT_REGEX = "[\\s*;.*]*";
	public static final String LABEL_REGEX = "\\s*[a-zA-Z]\\S*\\s*:";
	
	public static String getInstructionRegex() {
		String regex = "(";
		for (int i=0; i<Instruction.isa.length; i++) {
			regex += Instruction.isa[i] + "|" + Instruction.isa[i].toUpperCase();
			regex += (i != Instruction.isa.length - 1) ? "|" : "";
		}
		regex += ")";
		return regex;
	}
	
	public static String getRegisterRegex() {
		String regex = "(";
		for (int i=0; i<Instruction.regs.length; i++) {
			regex += Instruction.regs[i] + "|" + Instruction.regs[i].toUpperCase();
			regex += (i != Instruction.regs.length - 1) ? "|" : "";
		}
		regex += ")";
		return regex;
	}
	
}
