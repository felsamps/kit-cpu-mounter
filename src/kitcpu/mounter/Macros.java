package kitcpu.mounter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kitcpu.defs.CommonRegex;
import kitcpu.exceptions.IllegalMounterMacroException;

public class Macros {
	
	/* Methods to handle with ORG mounter macro */

	public static boolean isOrg(String line) {
		return line.matches("\\s*(org|ORG)\\s*\\d+" + CommonRegex.COMMENT_REGEX);
	}
	
	public static boolean isDB(String line) {
		return line.matches(CommonRegex.LABEL_REGEX + "\\s*(db|DB)\\s*\\d*" + CommonRegex.COMMENT_REGEX);
	}
	
	public static int getOrgPointer(String line) throws IllegalMounterMacroException {
		Pattern patt = Pattern.compile("\\d+");
		Matcher match = patt.matcher(line);
		int returnable = -1;		
		if(match.find()) {
			returnable = Integer.parseInt(line.substring(match.start(), match.end()));
		}
		else {
			throw new IllegalMounterMacroException("org", line);
		}
		return returnable;
		
	}
	
	public static String getLabel(String line) {
		return line.split("\\s*:\\s*")[0];
	}
	
	public static int getDBInitValue(String line) {
		Pattern patt = Pattern.compile("\\s+\\d+");
		Matcher match = patt.matcher(line);
		int returnable = -1;		
		if(match.find()) {
			returnable = Integer.parseInt(line.substring(match.start(), match.end()).replaceAll("\\s", ""));
		}
		else {
			returnable = 0; //default: intialize with 0
		}
		return returnable;
	}
	
}
