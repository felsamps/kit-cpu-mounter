package kitcpu.mounter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import kitcpu.exceptions.IllegalMounterMacroException;

public class Mounter {
    private static final int MEMORY_POSITIONS = 256;
    
    private TreeMap<String, Integer> symbTable;
    private TreeMap<Integer, String> lackTable;
    private byte[] memory;
    private ArrayList<String> code;
    
    public Mounter(File sourceFile) throws FileNotFoundException {
        this.symbTable = new TreeMap<>();
        this.lackTable = new TreeMap<>();
        
        this.memory = new byte[MEMORY_POSITIONS];
        
        Instruction.initIsaSizes();
        Instruction.initIsaOpcodes();
        Instruction.initRegCodes();
        
        this.readSourceFile(sourceFile);
    }
    
    private void readSourceFile(File sourceFile) throws FileNotFoundException {
        FileReader fr = new FileReader(sourceFile);
        Scanner scan = new Scanner(fr);
        
        this.code = new ArrayList<>();
        
        while(scan.hasNext()) {
            String line = scan.nextLine();
            if(!line.matches("\\s*")) { //remove blank lines
                this.code.add(line);
            }
        }
        
    }
    
    public void firstPass() throws IllegalMounterMacroException {
        int memCounter = 0;
        
        for(String line : this.code) {
            if(Macros.isOrg(line)) {
                memCounter = Macros.getOrgPointer(line);
            }
            else if(Macros.isDB(line)) {
                String label = Macros.getLabel(line);
                int initValue = Macros.getDBInitValue(line);
                
                this.insertSymbol(label, memCounter);
                this.setMemory(memCounter, initValue);
                memCounter ++;
            }
            else { /* instructions */
                Instruction inst = new Instruction(line);
                
                if(inst.hasLabel()) {
                    this.insertSymbol(inst.getLabel(), memCounter);
                }
                
                this.setMemory(memCounter++, inst.getByte0());
                
                if(inst.getIsaSize() == 2) { /* TODO modify it when other memory access modes will be supported */
                    String mem = inst.getMem();
                    this.insertLack(memCounter++, mem);
                }
                
            }
        }
        
    }
    
    
    public void secondPass() {
        for(Map.Entry<Integer,String> entry : this.lackTable.entrySet()) {
            Integer memPosition = entry.getKey();
            String memField = entry.getValue();
            
            if(symbTable.containsKey(memField)) {
                Integer memPositionOfLabel = symbTable.get(memField);
                this.memory[memPosition] = memPositionOfLabel.byteValue();
            }
        }
                
    }
    
    
    public void report() {
        System.out.println(this.symbTable.toString());
        System.out.println(this.lackTable.toString());
        
        int i=0;
        for(byte b : this.getMemory()) {
           
            Integer bI = (((int)b) >= 0) ? (int) b : ((int) b) + 256;
            System.out.println("MEM[" + i + "]=" + bI);

            i++;
        }
        
        
    }
    
    /* Methods to handle with mounter data structures */
    
    private void setMemory(int address, int value) {
        this.memory[address] = (byte) value;
        
    }
    
    private void insertSymbol(String label, int memCounter) {
        this.symbTable.put(label, memCounter);
    }
    
    private void insertLack(int memPosition, String label) {
        this.lackTable.put(memPosition, label);
    }

    /**
     * @return the memory
     */
    public byte[] getMemory() {
        return memory;
    }
    
    
}
