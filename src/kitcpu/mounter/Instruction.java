package kitcpu.mounter;

import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kitcpu.defs.CommonRegex;

public class Instruction {
	
	public static final String[] isa = {"nop", "str", "ldr", "add", "or", "and", "not", "sub", "jmp", "jn", "jz", "jc", "jsr", "in", "out", "hlt"};
	public static final int[] opcodes = {0, 16, 32, 48,	64,	80,	96, 112, 128, 144, 160, 176, 192, 208, 224, 255};
	public static final int[] sizes = {1, 2, 2, 2, 2, 2, 1, 2 ,2 ,2 ,2 ,2, 2, 2, 2, 1};
	public static final String[] regs = {"a", "b", "x"};
	public static final int[] rCodes = {0, 4, 8};
	
	private static TreeMap<String, Integer> isaSizes, isaOpcodes, regCodes;
		
	private String codeLine;
	private String inst;
	private String label;
	private String reg;
	private String mem;
	private byte byte0;
	
	
	public Instruction(String line) {
		this.codeLine = line;
		this.label = this.parseLabel();
		this.inst = this.parseInstruction();
		this.reg = this.parseRegister();
		this.mem = this.parseMem();
		
		this.generateByte0();
	}
	

	private String parseLabel() {
		String regex = CommonRegex.LABEL_REGEX;
		Matcher match = Pattern.compile(regex).matcher(this.codeLine);
		String returnable = null;
		
		
		if(match.find()) {
			returnable = this.codeLine.substring(match.start(), match.end()).replaceAll("\\s|:", "");
		}
		
		this.codeLine = this.codeLine.replaceAll(regex, "");
		return returnable;
	}

	private String parseInstruction() {		 
		this.codeLine = " " + this.codeLine;
		
		String regex = "\\s+" + CommonRegex.getInstructionRegex() + "(\\s+|$)";
		Matcher match = Pattern.compile(regex).matcher(this.codeLine);
		String returnable = null;
						
		if(match.find()) {
			returnable = this.codeLine.substring(match.start(), match.end()).replaceAll("\\s|:", "");
		}
		
		this.codeLine = this.codeLine.replaceAll(regex, "");
		
		return returnable;
	}
	
	private String parseRegister() {
		String returnable = null;
		
		//TODO: fix this
		if(!this.inst.equalsIgnoreCase("hlt") && !this.inst.equalsIgnoreCase("jmp") && !this.inst.equalsIgnoreCase("jn") && !this.inst.equalsIgnoreCase("jz") && !this.inst.equalsIgnoreCase("jc") && !this.inst.equalsIgnoreCase("in") && !this.inst.equalsIgnoreCase("out")) {
			this.codeLine = " " + this.codeLine; //work arround
			
			String regex = "\\s+" + CommonRegex.getRegisterRegex() + "(\\s+|$)";
			Matcher match = Pattern.compile(regex).matcher(this.codeLine);
			
			if(match.find()) {
				returnable = this.codeLine.substring(match.start(), match.end()).replaceAll("\\s", "");
			}
			
			this.codeLine = this.codeLine.replaceAll(regex, "");
		}
		
		if(returnable != null) {
			returnable = returnable.toLowerCase();
		}
		
		return returnable;
	}
	
	private String parseMem() {
		String returnable = null;
		
		this.codeLine = " " + this.codeLine;
	
		String regex = "\\s+[\\w]+(\\s+|$|;)";
		Matcher match = Pattern.compile(regex).matcher(this.codeLine);
		
		if(match.find()) {
			returnable = this.codeLine.substring(match.start(), match.end()).replaceAll("\\s|;", "");
		}
		
		this.codeLine = this.codeLine.replace(regex, "");
			
		return returnable;
	}

	//"\\s*[" + CommonRegex.LABEL_REGEX + "]?" +
	
	
	/*private String parseLabel() {
		return this.codeLine.split("\\s*:\\s*")[0];
	}*/

	private void generateByte0() {
		int genByte = Instruction.isaOpcodes.get(this.inst.toLowerCase());
		
		//TODO: fix this
		if(!this.inst.equalsIgnoreCase("hlt") && !this.inst.equalsIgnoreCase("jmp") && !this.inst.equalsIgnoreCase("jn") && !this.inst.equalsIgnoreCase("jz") && !this.inst.equalsIgnoreCase("in") && !this.inst.equalsIgnoreCase("out")) {

			genByte += Instruction.regCodes.get(this.reg.toLowerCase());
		}
		this.byte0 = (byte) genByte;		
	}

	public byte getByte0() {
		return byte0;
	}
	

	public String getCodeLine() {
		return this.codeLine;
	}

	public String getInst() {
		return this.inst;
	}

	public String getLabel() {
		return this.label;
	}

	public String getReg() {
		return this.reg;
	}

	public String getMem() {
		return this.mem;
	}
	
	public boolean hasLabel() {
		return this.label != null;
	}
	

	public static void initIsaSizes() {
		isaSizes = new TreeMap<String, Integer>() ;
		for (int i=0; i<isa.length; i++) {
			isaSizes.put(isa[i], sizes[i]);
		}
	}
	
	public int getIsaSize() {
		return isaSizes.get(this.inst.toLowerCase());
	}
	
	public static void initIsaOpcodes() {
		isaOpcodes = new TreeMap<String, Integer>() ;
		for (int i=0; i<isa.length; i++) {
			isaOpcodes.put(isa[i], opcodes[i]);
		}
	}
	
	public static void initRegCodes() {
		regCodes = new TreeMap<String, Integer>() ;
		for (int i=0; i<regs.length; i++) {
			regCodes.put(regs[i], rCodes[i]);
		}
	}
	
}
