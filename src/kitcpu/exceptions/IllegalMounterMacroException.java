package kitcpu.exceptions;

public class IllegalMounterMacroException extends Exception {
	private String macro;
	private String line;
	
	public IllegalMounterMacroException(String macro, String line) {
		this.line = line;
		this.macro = macro;
	}
	
	public String getErrorMessage() {
		return "Illegal construction for macro " + macro + " in line: " + line;
	}
}
