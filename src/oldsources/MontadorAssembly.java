
package oldsources;

import java.io.FileNotFoundException;


public class MontadorAssembly {

    
    public static void main(String[] args) {
        
        try {
            montador mnt = new montador();
            byte[] memoria = mnt.monta();
            
            System.out.println("Memória:");
            for (int i = 0; i < memoria.length; i++) {
                System.out.println(memoria[i] < 0 ? memoria[i] + 256 : memoria[i]);
                
            }
        }
        catch(FileNotFoundException fnfe) {
            System.out.println("Arquivo não encontrado!");
        }
        
    }
    
}
