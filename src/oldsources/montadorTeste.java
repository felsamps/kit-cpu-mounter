package oldsources;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import static java.lang.Character.isDigit;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import javax.swing.JFileChooser;


public class montadorTeste {

    ArrayList<Integer> comandos = new ArrayList();
    ArrayList<Integer> variaveis = new ArrayList();
    ArrayList<Label> labels = new ArrayList();
    Map<String, Integer> var = new HashMap<>();
    Map<String, Integer> label = new HashMap<>();
    byte[] mem = new byte[256];
    int contador = 128;
    int contadorComandos = 0;

    public void db(String linha) {
        String nome = linha.split("\\s*:\\s*")[0];
        String valor = linha.split("[d|D][B|b]\\s+")[1];
        var.put(nome, contador);
        contador++;
        variaveis.add(parseInt(valor));
    }

    public void nop() {
        comandos.add(12);
        contadorComandos++;
    }

    public void str(String linha) {
        String registrador = linha.split("\\s+")[1];
        String variavel = linha.split("\\s+")[2];
        if (registrador.matches("[A|a]")) {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(16);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(16);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else if(registrador.matches("[B|b]")){
            if (isDigit(variavel.charAt(0))) {
                comandos.add(20);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(20);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else{
            if (isDigit(variavel.charAt(0))) {
                comandos.add(24);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(24);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        }
    }
    
    public void ldr(String linha){
        String registrador = linha.split(" ")[1];
        String variavel = linha.split(" ")[2];
        if (registrador.matches("[A|a]")) {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(32);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(32);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else if(registrador.matches("[B|b]")){
            if (isDigit(variavel.charAt(0))) {
                comandos.add(36);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(36);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(40);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(40);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        }
   }
    
    public void add(String linha){
        String registrador = linha.split(" ")[1];
        String variavel = linha.split(" ")[2];
        if (registrador.matches("[A|a]")) {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(48);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(48);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else if(registrador.matches("[B|b]")){
            if (isDigit(variavel.charAt(0))) {
                comandos.add(52);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(52);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(56);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(56);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        }
    }
    
    public void or(String linha){
        String registrador = linha.split(" ")[1];
        String variavel = linha.split(" ")[2];
        if (registrador.matches("[A|a]")) {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(64);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(64);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else if(registrador.matches("[B|b]")){
            if (isDigit(variavel.charAt(0))) {
                comandos.add(68);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(68);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(72);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(72);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        }
    }
    
    public void and(String linha){
        String registrador = linha.split(" ")[1];
        String variavel = linha.split(" ")[2];
        if (registrador.matches("[A|a]")) {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(80);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(80);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else if(registrador.matches("[B|b]")){
            if (isDigit(variavel.charAt(0))) {
                comandos.add(84);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(84);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(88);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(88);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        }    
    } 
    
    public void not(String linha){
        String registrador = linha.split(" ")[1];
        String variavel = linha.split(" ")[2];
        if (registrador.matches("[A|a]")) {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(96);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(96);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else if(registrador.matches("[B|b]")){
            if (isDigit(variavel.charAt(0))) {
                comandos.add(100);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(100);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(104);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(104);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        }
    }
    
    public void sub(String linha){
        String registrador = linha.split(" ")[1];
        String variavel = linha.split(" ")[2];
        if (registrador.matches("[A|a]")) {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(112);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(112);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else if(registrador.matches("[B|b]")){
            if (isDigit(variavel.charAt(0))) {
                comandos.add(116);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(116);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(120);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(120);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        }
    }
    
    public void neg(String linha){
        String registrador = linha.split(" ")[1];
        String variavel = linha.split(" ")[2];
        if (registrador.matches("[A|a]")) {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(212);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(212);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else if(registrador.matches("[B|b]")){
            if (isDigit(variavel.charAt(0))) {
                comandos.add(216);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(216);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(220);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(220);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        }
    }
    
    public void shr(String linha){
        String registrador = linha.split(" ")[1];
        String variavel = linha.split(" ")[2];
        if (registrador.matches("[A|a]")) {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(228);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(228);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else if(registrador.matches("[B|b]")){
            if (isDigit(variavel.charAt(0))) {
                comandos.add(232);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(232);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        } else {
            if (isDigit(variavel.charAt(0))) {
                comandos.add(236);
                contadorComandos++;
                comandos.add(parseInt(variavel));
                contadorComandos++;
            } else {
                comandos.add(236);
                contadorComandos++;
                comandos.add(var.get(variavel));
                contadorComandos++;
            }
        }
    }
    
    public void jmp(String linha){
        String lab = linha.split("\\s+")[1];
        comandos.add(140);
        contadorComandos++;
        Label lab1 = new Label(contadorComandos, lab);
        labels.add(lab1);
        comandos.add(256);
        contadorComandos++;
    }
    
    public void jn(String linha){
        String lab = linha.split("\\s+")[1];
        comandos.add(156);
        contadorComandos++;
        Label lab1 = new Label(contadorComandos, lab);
        labels.add(lab1);
        comandos.add(256);
        contadorComandos++;
    }
    
    public void jz(String linha){
        String lab = linha.split("\\s+")[1];
        comandos.add(172);
        contadorComandos++;
        Label lab1 = new Label(contadorComandos, lab);
        labels.add(lab1);
        comandos.add(256);
        contadorComandos++;
    }
    
    public void jc(String linha){
        String lab = linha.split("\\s+")[1];
        comandos.add(188);
        contadorComandos++;
        Label lab1 = new Label(contadorComandos, lab);
        labels.add(lab1);
        comandos.add(256);
        contadorComandos++;
    }
    public void jsr(String linha){
        String lab = linha.split("\\s+")[1];
        comandos.add(204);
        contadorComandos++;
        Label lab1 = new Label(contadorComandos, lab);
        labels.add(lab1);
        comandos.add(256);
        contadorComandos++;
    }
    
    public void hlt(){
        comandos.add(252);
        contadorComandos++;
    }
    
    public String  label(String linha){
        if (linha.contains(":")){
            String lab = linha.split("\\s*:\\s*")[0];
            label.put(lab, contadorComandos);
            return linha.split("\\s*:\\s*")[1];
        }else{
            return linha;
        }     
        
        
    }
    
    public byte[] monta(FileReader x) throws FileNotFoundException {
            Scanner s = new Scanner(x);
            String linha;
            
            while (s.hasNext()) {
                linha = s.nextLine();

                if (linha.matches("\\s*\\S+\\s*:\\s*[d|D][B|b]\\s+\\d+")) {
                    this.db(linha);
                } else {
                    //Se for algum comando
                    String linhaSemLabel = this.label(linha);
                    
                    if (linha.matches("\\s*[N|n][O|o][P|p]\\s*")) {
                        this.nop();
                    } else if (linha.matches("\\s*[S|s][T|t][R|r]\\s+\\w\\s+\\S+")) {
                        this.str(linha);
                    } else if (linha.matches("\\s*[L|l][D|d][R|r]\\s+\\w\\s+\\S+")) {
                        this.ldr(linha);
                    } else if (linha.matches("\\s*[A|a][D|d][D|d]\\s+\\w\\s+\\S+")) {
                        this.add(linha);
                    } else if (linha.matches("\\s*[O|o][R|r]\\s+\\w\\s+\\S+")) {
                        this.or(linha);
                    } else if (linha.matches("\\s*[A|a][N|n][D|d]\\s+\\w\\s+\\S+")) {
                        this.and(linha);
                    } else if (linha.matches("\\s*[N|n][O|o][T|t]\\s+\\w\\s+\\S+")) {
                        this.not(linha);
                    } else if (linha.matches("\\s*[S|s][U|u][B|b]\\s+\\w\\s+\\S+")) {
                        this.sub(linha);
                    } else if (linha.matches("\\s*[N|n][E|e][G|g]\\s+\\w\\s+\\S+")) {
                        this.neg(linha);
                    } else if (linha.matches("\\s*[S|s][H|h][R|r]\\s+\\w\\s+\\S+")) {
                        this.shr(linha);
                    } else if (linha.matches("\\s*[J|j][M|m][P|p]\\s+\\S+")) {
                        this.jmp(linha);
                    } else if ("jn".equals(linha.split(" ")[0])) {
                        this.jn(linha);
                    } else if ("jz".equals(linha.split(" ")[0])) {
                        this.jz(linha);
                    } else if ("jc".equals(linha.split(" ")[0])) {
                        this.jc(linha);
                    } else if ("jsr".equals(linha.split(" ")[0])) {
                        this.jsr(linha);
                    } else if ("hlt".equals(linha)) {
                        this.hlt();
                    } else{
                        //
                    }
                }
        }
        int cont = 0;
        for (int i = 0; i < comandos.size(); i++) {
            if (comandos.get(i) == 256) {
                String lab;
                lab = labels.get(cont).label;
                comandos.remove(i);
                comandos.add(i, label.get(lab));
                cont++;
            }
        }
        for (int i = 0; i < 256; i++) {
            mem[i] = (byte) 0;
        }
        for (int i = 0; i < comandos.size(); i++) {
            int p = comandos.get(i);
            mem[i] = (byte) p;
        }
        for (int i = 128; i < var.size() + 128; i++) {
            int p = variaveis.get(i - 128);
            mem[i] = (byte) p;
        }
        for (int i = 0; i < 256; i++) {
            System.out.print(mem[i] + ", "); //imprime o vetor na saída
        }
        return mem; // retorna o vetor
    }
}
