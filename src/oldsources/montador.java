package oldsources;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import static java.lang.Character.isDigit;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import javax.swing.JFileChooser;


public class montador {
    public byte[] monta() throws FileNotFoundException{
    	ArrayList<Integer> comandos = new ArrayList();
        ArrayList<Integer> variaveis = new ArrayList();
        ArrayList<Label> labels = new ArrayList();
        byte[] mem = new byte[256];
        for(int i = 0; i < 256; i++){
            mem[i] = (byte) 0;
        }

        Map<String, Integer> var = new HashMap<>();
        Map<String, Integer> label = new HashMap<>();
        int contador = 128;
        int contadorComandos = 0;

        JFileChooser j = new JFileChooser("/home/fmsampaio/Google Drive/Pesquisa/Projeto Kit Didático/Códigos Assembly - Entrada");
        int fileChooser = j.showOpenDialog(null);
        if (fileChooser == JFileChooser.APPROVE_OPTION) {
            File a;
            FileReader x;
            a = new File(j.getSelectedFile().getPath());
            x = new FileReader(a);
            Scanner s = new Scanner(x);
            String linha = "";
            boolean flag = true;
            while (s.hasNext()) {
                if (flag == true) {
                    linha = s.nextLine();
                } else {
                    flag = true;
                }
                if (linha.contains("db")) {
                    String nome = linha.split(":")[0];
                    String valor = linha.split("db ")[1];
                    var.put(nome, contador);
                    contador++;
                    variaveis.add(parseInt(valor));

                } else if ("nop".equals(linha.split(" ")[0])) {
                    comandos.add(12);
                    contadorComandos++;
                } else if ("str".equals(linha.split(" ")[0])) {
                    String registrador = linha.split(" ")[1];
                    String variavel = linha.split(" ")[2];
                    if ("a".equals(registrador)) {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(16);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(16);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    } else {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(20);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(20);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    }
                } else if ("ldr".equals(linha.split(" ")[0])) {
                    String registrador = linha.split(" ")[1];
                    String variavel = linha.split(" ")[2];
                    if ("a".equals(registrador)) {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(32);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(32);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    } else {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(36);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(36);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    }
                } else if ("add".equals(linha.split(" ")[0])) {
                    String registrador = linha.split(" ")[1];
                    String variavel = linha.split(" ")[2];
                    if ("a".equals(registrador)) {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(48);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(48);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    } else {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(52);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(52);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    }
                } else if ("or".equals(linha.split(" ")[0])) {
                    String registrador = linha.split(" ")[1];
                    String variavel = linha.split(" ")[2];
                    if ("a".equals(registrador)) {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(64);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(64);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    } else {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(68);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(68);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    }
                } else if ("and".equals(linha.split(" ")[0])) {
                    String registrador = linha.split(" ")[1];
                    String variavel = linha.split(" ")[2];
                    if ("a".equals(registrador)) {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(80);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(80);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    } else {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(84);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(84);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    }
                } else if ("not".equals(linha.split(" ")[0])) {
                    String registrador = linha.split(" ")[1];
                    String variavel = linha.split(" ")[2];
                    if ("a".equals(registrador)) {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(96);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(96);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    } else {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(100);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(100);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;

                        }
                    }
                } else if ("sub".equals(linha.split(" ")[0])) {
                    String registrador = linha.split(" ")[1];
                    String variavel = linha.split(" ")[2];
                    if ("a".equals(registrador)) {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(112);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(112);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    } else {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(116);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(116);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    }
                } else if ("neg".equals(linha.split(" ")[0])) {
                    String registrador = linha.split(" ")[1];
                    String variavel = linha.split(" ")[2];
                    if ("a".equals(registrador)) {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(208);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(208);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    } else {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(212);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(212);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    }
                } else if ("shr".equals(linha.split(" ")[0])) {
                    String registrador = linha.split(" ")[1];
                    String variavel = linha.split(" ")[2];
                    if ("a".equals(registrador)) {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(224);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(224);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    } else {
                        if (isDigit(variavel.charAt(0))) {
                            comandos.add(228);
                            contadorComandos++;
                            comandos.add(parseInt(variavel));
                            contadorComandos++;
                        } else {
                            comandos.add(228);
                            contadorComandos++;
                            comandos.add(var.get(variavel));
                            contadorComandos++;
                        }
                    }
                } else if ("jmp".equals(linha.split(" ")[0])) {
                    String lab = linha.split(" ")[1];
                    comandos.add(140);
                    contadorComandos++;
                    Label lab1 = new Label(contadorComandos, lab);
                    labels.add(lab1);
                    comandos.add(256);
                    contadorComandos++;
                } else if ("jn".equals(linha.split(" ")[0])) {
                    String lab = linha.split(" ")[1];
                    comandos.add(156);
                    contadorComandos++;
                    Label lab1 = new Label(contadorComandos, lab);
                    labels.add(lab1);
                    comandos.add(256);
                    contadorComandos++;
                } else if ("jz".equals(linha.split(" ")[0])) {
                    String lab = linha.split(" ")[1];
                    comandos.add(172);
                    contadorComandos++;
                    Label lab1 = new Label(contadorComandos, lab);
                    labels.add(lab1);
                    comandos.add(256);
                    contadorComandos++;
                } else if ("jc".equals(linha.split(" ")[0])) {
                    String lab = linha.split(" ")[1];
                    comandos.add(188);
                    contadorComandos++;
                    Label lab1 = new Label(contadorComandos, lab);
                    labels.add(lab1);
                    comandos.add(256);
                    contadorComandos++;
                } else if ("jsr".equals(linha.split(" ")[0])) {
                    String lab = linha.split(" ")[1];
                    comandos.add(204);
                    contadorComandos++;
                    Label lab1 = new Label(contadorComandos, lab);
                    labels.add(lab1);
                    comandos.add(256);
                    contadorComandos++;
                } else if ("hlt".equals(linha)) {
                    comandos.add(252);
                    contadorComandos++;
                } else if (linha.isEmpty()) {
                    //
                } else {
                    String lab = linha.split(":")[0];
                    label.put(lab, contadorComandos);
                    linha = linha.split(":")[1];
                    flag = false;
                }
            }
        }
        int cont = 0;
        for (int i = 0; i < comandos.size(); i++) {
            if (comandos.get(i) == 256) {
                String lab;
                lab = labels.get(cont).label;
                comandos.remove(i);
                comandos.add(i, label.get(lab));
                cont++;
            }
        }
        for (int i = 0; i < comandos.size(); i++) {
            int p = comandos.get(i);
            mem[i] = (byte) p;
        }
        for (int i = 128; i < var.size() + 128; i++) {
            int p = variaveis.get(i-128);
            mem[i] = (byte) p;
        }
        for (int i = 0; i < 256; i++) {
            System.out.print(mem[i] + ", ");
        }
        return mem;
    }
}

